@extends('layout')

@section('content')
    <div class="container">
        <h3>Questions</h3>
        <a href="{{ route('tasks.create') }}" class="btn btn-success">Create</a>
        <div class="row">

            <div class="col-md-10 col-md-offset-1">
                <table class="table">
                    <thread>
                        <tr>
                            <td>ID</td>
                            <td>Title</td>
                            <td>Description</td>
                            <td>Actions</td>

                        </tr>
                    </thread>
                    <tbody>
                    @foreach($tasks as $task)
                    <tr>
                        <td>{{$task->id}}</td>
                        <td>{{$task->title}}</td>
                        <td>{{$task->description}}</td>
                        <td>
                            <a href="{{route('tasks.show',$task->id)}}">

                                <i class="glyphicon glyphicon-eye-open"></i>

                            </a>


                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    <span>Total tasks: {{ \App\Task::count() }}</span>
                </div>
                <div class="text-center">
                    {!! $tasks->links() !!}
                </div>


            </div>
        </div>
    </div>
    @endsection